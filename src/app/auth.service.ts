import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { Observable } from 'rxjs';
import { AngularFireDatabase } from '@angular/fire/database';


@Injectable({
  providedIn: 'root'
})
export class AuthService {

  register(email:string, password:string)
  {
    //console.log("hiiii");
    return this.fireBaseAuth//משתנה שפתחנו בקונסטרקטור
              .auth.createUserWithEmailAndPassword(email, password);//, פונקציה שמורה של פיירבייס
  }

  updateProfile(user, name:string)
  {
    user.updateProfile({displayName:name, photoURL:''});//הפונקציה לא מחזירה תשובה
  }

  login(email:string, password:string)
  {
    return this.
          fireBaseAuth.
          auth.
          signInWithEmailAndPassword(email,password);
  }

  logout()
  {
    return this
          .fireBaseAuth
          .auth
          .signOut();
  }
  addUser(user, name: string)
  {
    let uid = user.uid;
    let ref = this.db.database.ref('/');
    ref.child('users').child(uid).push({'name':name});
  }

  

  user: Observable<firebase.User>;//, להוסיף אימפורט ! כדי לדעת אם היוזר מחובר או לא
  
  constructor(private fireBaseAuth: AngularFireAuth, 
              private db:AngularFireDatabase) 
  {
    this.user = fireBaseAuth.authState;//כדי לדעת אם היוזר מחובר או לא
  }
}
