import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {


  email: string;
  password: string;
  name: string;
  code = '';
  message = '';
  hide = true;
  register()
  {
    this.authService.register(this.email,this.password)//שלחנו את המייל וססמא
    .then(value => { 
          this.authService.updateProfile(value.user,this.name);//מוסיף ליוזר את השם שלו
          this.authService.addUser(value.user,this.name); //שומר את היוזר בדטה בייס
    }).then(value =>{
      this.router.navigate(['/']);// העברה לעמוד הבא
    }).catch(err => {//במידה ויש שגיאה אוטומטי יגיע לפה
      this.code = err.code;//מכניסים לתוך ארור את השגיאה שקיבלנו
      this.message = err.message;
      console.log(err);
    })
    //console.log("register " + this.name + ' ' + this.email + ' ' + this.password);
  }

  constructor(private authService:AuthService, private router:Router) { }

  ngOnInit() {
  }

}
