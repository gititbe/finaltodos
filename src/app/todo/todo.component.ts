import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { TodosService } from '../todos.service';


@Component({
  selector: 'todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.css']
})

export class TodoComponent implements OnInit {

  @Input() data:any;//אב לבן
  @Output() myButtonClicked = new EventEmitter<any>();//בן לאב

  text;
  tempText;
  key;

  showTheButton = false;
  showEditField = false;

  showEdit()
  {
    this.tempText = this.text;//שומרים את הטקסט בצד
    this.showTheButton = false;// מסתירים את הכפתור של מחיקה
    this.showEditField = true;//מציגים את האפשרות לערוך
  }

  save()
  {
    this.todosService.updateTodo(this.key,this.text);
    this.showEditField = false;//נוריד את האפשרות לעריכה
  }

  cancel()
  {
    this.text = this.tempText;//ברגע שבחרנו לבטל את העריכה נחזיר את הטקסט ששמרנו בצד למשתנה טקסט
    this.showEditField = false;//נוריד את האפשרות לעריכה
  }

  showButton()
  {
    this.showTheButton = true;
  }

  hideButton()
  {
    this.showTheButton = false;
  }

  deleteTodo()
  {
    this.todosService.deleteTodo(this.key);
  }

  constructor(private todosService: TodosService) { }

  ngOnInit() {

     this.text = this.data.text;
     this.key = this.data.$key;
  }

}
